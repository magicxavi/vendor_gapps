TARGET_MINIMAL_APPS ?= false

$(call inherit-product, vendor/gapps/common-blobs.mk)

# Include package overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += vendor/gapps/overlay
DEVICE_PACKAGE_OVERLAYS += \
    vendor/gapps/overlay/common/

# framework
PRODUCT_PACKAGES += \
    com.google.android.maps \
    com.google.android.media.effects \
    com.google.widevine.software.drm

PRODUCT_PACKAGES += \
    com.google.android.dialer.support

# app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    FaceLock \
    GoogleContactsSyncAdapter \
    GoogleExtShared \
    LatinIMEGooglePrebuilt \
    PrebuiltDeskClockGoogle

PRODUCT_PACKAGES += \
    PrebuiltBugle

ifeq ($(TARGET_MINIMAL_APPS),false)

ifeq ($(TARGET_INCLUDE_STOCK_ARCORE),true)
PRODUCT_PACKAGES += \
    arcore
endif

PRODUCT_PACKAGES += \
    CalendarGooglePrebuilt
endif

# priv-app
PRODUCT_PACKAGES += \
    AndroidPlatformServices \
    ConfigUpdater \
    GoogleBackupTransport \
    GoogleContacts \
    GoogleExtServices \
    GoogleOneTimeInitializer \
    GoogleServicesFramework \
    Phonesky \
    PrebuiltGmsCorePi \
    SetupWizard \
    Turbo \
    Velvet \
	PhotosGO \
	PixelSetupWizard \
	GoogleRestore

PRODUCT_PACKAGES += \
    GoogleDialer
